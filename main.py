from pathlib import Path
import tag_json

path_to_tag_file = Path.home() / 'Desktop/test_files_for_tag_system/tag_file.json'
tag_list = tag_json.json_open(path_to_tag_file)

item = 'gulfisk'
item_search = tag_json.search_for_file(tag_list, item)
if item_search:
    print(f'{item} is tagged at {item_search}\n')
else:
    print(f'{item} is not tagged anywhere\n')

tag = 'brødre'
item_list = ['Emil', 'Mikael', 'Gunleik', 'Eirik']
tag_json.create_tag(tag_list, tag)

for item in item_list:
    tag_json.tag_file(tag_list, tag, item)
    if item == 'Eirik':
        tag_json.tag_file(tag_list, 'pet', item)

tag_json.untag_file(tag_list, tag, 'Eirik')

print(tag_json.list_tags(tag_list))
print(tag_json.list_items_in_tag(tag_list, ['pet', 'fisk']))
print(tag_list)

tag_json.json_write(path_to_tag_file, tag_list)
