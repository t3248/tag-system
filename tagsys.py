#! /bin/python3
from pathlib import Path
import argparse
import tag_json

cmd_parser = argparse.ArgumentParser(description='manage files whit tags')
tag_ops = cmd_parser.add_argument_group('tag operations', 'operations to manage tags')
tag_ops.add_argument('-T', '--tags_available',
                     action='store_true',
                     help='list all available tags')
tag_ops.add_argument('-L', '--list_files',
                     help='list the files in tag')
tag_ops.add_argument('-I', '--is_tag',
                     help='check if tag exist')
tag_ops.add_argument('-C', '--create_tag',
                     help='create a new tag')
tag_ops.add_argument('-R', '--Remove_tag',
                     help='remove an existing tag')

file_ops = cmd_parser.add_argument_group('file operations', 'operations to manage files')
file_ops.add_argument('-f', '--find_tags',
                      help='find all tags on the file')
file_ops.add_argument('-t', '--tag_file',
                      nargs='+',
                      help='tag a file')
file_ops.add_argument('-u', '--untag_file',
                      nargs='+',
                      help='untag a file')

args = cmd_parser.parse_args()

path_to_tag_file = Path.home() / 'Desktop/test_files_for_tag_system/tag_file.json'
tag_list = tag_json.json_open(path_to_tag_file)

if args.tags_available:
    print(tag_json.list_tags(tag_list))

if args.list_files:
    for x in tag_json.list_items_in_tag(tag_list, args.list_files):
        print(x)

if args.is_tag:
    if tag_json.is_tag(tag_list, args.is_tag):
        print(f'{args.is_tag} exist')
    else:
        print(f"{args.is_tag} don't exist")

if args.create_tag:
    tag_json.create_tag(tag_list, args.create_tag)
    print(f'{args.create_tag} is now created')

if args.Remove_tag:
    tag_json.remove_tag(tag_list, args.Remove_tag)

if args.tag_file:
    for x in args.tag_file[1::]:
        tag_json.tag_file(tag_list, x, args.tag_file[0])

if args.untag_file:
    for x in args.untag_file[1::]:
        tag_json.untag_file(tag_list, x, args.untag_file[0])

if args.find_tags:
    res = tag_json.search_for_file(tag_list, args.find_tags)
    if res:
        print(res)
    else:
        print(f'file "{args.find_tags}" not in tagsystem')

tag_json.json_write(path_to_tag_file, tag_list)
